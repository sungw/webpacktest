let Koa = require('koa');
let path = require('path');
const fs = require('fs')    //文件操作
const static = require('koa-static');   //静态资源服务插件
var Router = require('koa-router');
var router = new Router();

let app = new Koa()
const staticPath = 'dist'       //根基自己的目录配置文件路径
// 配置静态web服务的中间件
app.use(static(
  path.join( __dirname,  staticPath)
));
 
router.get('/',async (ctx)=>{
    ctx.response.type = 'html'
    ctx.response.body = fs.createReadStream(path.resolve(__dirname, 'index.html'))  //替换自己需要的路径
})
 
/*启动路由*/
app.use(router.routes())   
.use(router.allowedMethods());     
app.listen(8080);

console.log('直接访问 http://127.0.0.1:8080')
