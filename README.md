#### webpack练习

> 读取src下的所有路径js文件，并打包到dist文件夹下

> 每次生成js 文件都删除原有的dist文件夹，并生成新的dist文件夹以及js文件

> 新加server本地服务，依赖koa服务、router，npm run dev后，访问 http://127.0.0.1:8080 即可访问src目录下打包好的js文件

> webpack只能打包js文件，因为入口文件就是js文件；因此想要打包非js文件，都需要引入模块化方式引入文件

webpack 提供的各种loader，就可以解决这种问题

