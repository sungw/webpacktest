const path = require('path');
const glob = require('glob');

let root = path.resolve(__dirname, '..', 'src');
let files = [
  ...glob.sync(path.join(root, '/*/index.js'))
];
let entrys = {};
files.forEach(v => {
  let name = v.replace(root, '').replace(/\/index\.js$/, '').replace(/\.js$/, '');
  name = name.replace(/^\/+/, '');
  entrys[name] = v;
  // entrys.push(name);
});
module.exports = entrys;