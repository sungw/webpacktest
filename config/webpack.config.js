const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
let entries = require('./entries');
// 删除dist

const fsPath = require('fs-path');
fsPath.remove(path.join(__dirname, '..', 'server/dist/'), () => {
  fsPath.mkdir(path.join(__dirname, '..', 'server/dist/'));
});

const pageExtractCssArray = [];
for(let item in entries) {
  console.log(path.resolve(__dirname, '..', `server/dist/css/${item}.css`));
  pageExtractCssArray.push(new ExtractTextPlugin(path.resolve(__dirname, '..', `server/dist/css/${item}.css`)))
}

// config
module.exports = {
  mode: 'development',
  entry: entries,
  output: {
    path: path.resolve(__dirname, '..', 'server/dist/js'),
    filename: '[name].js',
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel-loader"
      // test 符合此正则规则的文件，运用 loader 去进行处理，除了exclude 中指定的内容
    },{
      test: /\.css$/,
      use: [ 'style-loader', 'css-loader' ]
    }]
  },
  plugins: [
    ...pageExtractCssArray
  ]
};